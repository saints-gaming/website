// @flow
import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

const HomePartial = () => <div>Home page</div>;

const mapStateToProps = state => ({ home: state.home });
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(HomePartial)
);
