// @flow
import React from "react";
import { Helmet } from "react-helmet";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { actions } from "saints-kernel";

const AccountPartial = () =>
  <div>
    <Helmet>
      <title>Account</title>
    </Helmet>
    Account page
  </div>;

const mapStateToProps = state => ({ user: state.user });
const mapDispatchToProps = dispatch =>
  bindActionCreators(actions.user, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AccountPartial)
);
