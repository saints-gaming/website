// @flow
/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import { actions } from "saints-kernel";
import LoginForm from "../../components/Forms/LoginForm";

class LoginPartial extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
  }

  login(data) {
    this.props.doLogin(data);
  }

  render() {
    return (
      <div className="login-page">
        <Helmet>
          <title>Login</title>
        </Helmet>
        {this.props.user.authenticated && <Redirect to="/" />}
        <div className="uk-card uk-card-default uk-card-body">
          <LoginForm onSubmit={this.login} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ user: state.user });
const mapDispatchToProps = dispatch =>
  bindActionCreators(actions.user, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(LoginPartial)
);
