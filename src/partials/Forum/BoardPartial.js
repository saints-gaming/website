// @flow
/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { actions } from "saints-kernel";
import ThreadForm from "../../components/Forms/ThreadForm";

class BoardPartial extends Component {
  constructor(props) {
    super(props);
    this.create = this.create.bind(this);
  }

  componentWillMount() {
    this.props.getBoard(this.props.match.params.board);
  }

  create(data) {
    const toSend = data;
    toSend.board = this.props.forum.board._id;
    this.props.createThread(toSend);
  }

  render() {
    const { forum } = this.props;
    const threads = forum.board.threads ? forum.board.threads : false;
    return (
      <div className="uk-container uk-text-left">
        <h1>
          {forum.board.name}
        </h1>
        <div className="uk-card uk-card-default uk-padding uk-margin-top">
          <ThreadForm onSubmit={this.create} />
        </div>
        {threads &&
          threads.data
            .map(thread =>
              <div key={thread._id}>
                {thread.subject} by{" "}
                {thread.author.roles.filter(role =>
                  ["admin", "super-admin"].indexOf(role)
                ).length
                  ? <span className="uk-text-primary">
                      {thread.author.username}
                    </span>
                  : thread.author.username}
              </div>
            )
            .sort()}
      </div>
    );
  }
}

BoardPartial.propTypes = {
  createThread: PropTypes.func.isRequired,
  getBoard: PropTypes.func.isRequired
};

const mapStateToProps = (state = { boards: [] }) => ({ forum: state.forum });
const mapDispatchToProps = dispatch =>
  bindActionCreators(actions.forum, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(BoardPartial)
);
