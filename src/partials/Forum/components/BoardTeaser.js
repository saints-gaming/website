// @flow
import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const BoardTeaser = ({ name, description, slug }) =>
  <div>
    <h3>
      <Link to={`/forum/${slug}`}>
        {name}
      </Link>
    </h3>
    <div>
      {description}
    </div>
  </div>;

BoardTeaser.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  slug: PropTypes.string.isRequired
};

BoardTeaser.defaultProps = {
  description: ""
};

export default BoardTeaser;
