// @flow
/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { actions } from "saints-kernel";
import BoardTeaser from "./components/BoardTeaser";
import BoardForm from "../../components/Forms/BoardForm";

class ForumPartial extends Component {
  constructor(props) {
    super(props);
    // onInit
    props.getBoards();
    this.create = this.create.bind(this);
  }

  create(data) {
    this.props.createBoard(data);
  }

  render() {
    const { forum } = this.props;
    return (
      <div className="uk-container uk-text-left">
        <Helmet>
          <title>Forum</title>
        </Helmet>
        <div className="uk-card uk-card-default uk-padding uk-margin-top">
          <BoardForm onSubmit={this.create} />
        </div>
        <ul className="uk-list">
          {forum.boards &&
            Object.keys(forum.boards).map(key =>
              <li key={forum.boards[key]._id}>
                <BoardTeaser {...forum.boards[key]} />
              </li>
            )}
          {!Object.keys(forum.boards).length && <li>No boards</li>}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state = { boards: [] }) => ({ forum: state.forum });
const mapDispatchToProps = dispatch =>
  bindActionCreators(actions.forum, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ForumPartial)
);
