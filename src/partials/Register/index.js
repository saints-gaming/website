// @flow
/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import { actions } from "saints-kernel";
import RegisterForm from "../../components/Forms/RegisterForm";

class RegisterPartial extends Component {
  constructor(props) {
    super(props);
    this.register = this.register.bind(this);
  }

  register(data) {
    this.props.createUser(data);
  }

  render() {
    return (
      <div className="register-page">
        <Helmet>
          <title>Register</title>
        </Helmet>
        {this.props.user.authenticated && <Redirect to="/" />}
        <div className="uk-card uk-card-default uk-card-body">
          <RegisterForm onSubmit={this.register} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ user: state.user });
const mapDispatchToProps = dispatch =>
  bindActionCreators(actions.user, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(RegisterPartial)
);
