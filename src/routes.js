// @flow
import React from "react";
import { Helmet } from "react-helmet";
import { Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "react-router-redux";
import { history, store } from "saints-kernel";
import App from "./App";
import LoginPartial from "./partials/Login";
import RegisterPartial from "./partials/Register";

const Routes = () =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Helmet titleTemplate="%s | Saints Gaming" defaultTitle="Saints Gaming">
          <meta charSet="utf-8" />
        </Helmet>

        <Switch>
          <Route exact path="/login" component={LoginPartial} />
          <Route exact path="/register" component={RegisterPartial} />
          <Route path="/" component={App} />
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>;

export default Routes;
