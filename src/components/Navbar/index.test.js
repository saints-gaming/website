/* global it describe */
import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import Navbar from "./index";

const testMenu = [{ name: "Test", link: "/test", key: "test" }];

describe("Navbar", () => {
  it("should render without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <MemoryRouter>
        <Navbar menu={testMenu} />
      </MemoryRouter>,
      div
    );
  });
});
