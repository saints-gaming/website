import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import NavLink from "./components/NavLink";
import { Icon } from "../../components/Uikit";

const Navbar = ({ auth, logout, menu, user }) =>
  <nav className="uk-navbar uk-navbar-container">
    <div className="uk-navbar-left uk-hidden-notouch">
      <ul className="uk-navbar-nav">
        <li>
          <a className="uk-navbar-toggle">
            <Icon icon="menu" />
          </a>{" "}
        </li>
      </ul>
    </div>
    <div className="uk-navbar-left uk-hidden-touch">
      <Link to="" className="uk-navbar-item uk-logo">
        Saints Gaming
      </Link>
      <ul className="uk-navbar-nav">
        {menu.map(item =>
          <NavLink
            key={item.key}
            activeClassName="uk-active"
            exact={item.exact}
            to={item.link}
          >
            {item.label}
          </NavLink>
        )}
      </ul>
    </div>
    <div className="uk-navbar-right">
      {auth
        ? <ul className="uk-navbar-nav">
            <NavLink to="/account">
              <Icon icon="user" className="uk-margin-small-right" />
              {user}
            </NavLink>
            <li>
              <a onClick={logout}>
                <Icon icon="signOut" />
              </a>
            </li>
          </ul>
        : <ul className="uk-navbar-nav">
            <NavLink to="/login">Login</NavLink>
            <NavLink to="/register">Register</NavLink>
          </ul>}
    </div>
  </nav>;

const item = {
  exact: PropTypes.bool,
  key: PropTypes.string.isRequired,
  label: PropTypes.element.isRequired,
  link: PropTypes.string.isRequired
};

Navbar.propTypes = {
  menu: PropTypes.arrayOf(PropTypes.shape(item).isRequired).isRequired,
  auth: PropTypes.bool.isRequired,
  user: PropTypes.string,
  logout: PropTypes.func.isRequired
};

Navbar.defaultProps = {
  user: ""
};

export default Navbar;
