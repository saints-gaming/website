// @flow
/* eslint react/prop-types: 0 */
/* eslint react/no-unused-prop-types: 0 */
import React from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";

import { Input, Editor } from "../Fields";

const validate = values => {
  const errors = {};
  if (!values.name) {
    errors.name = "Required";
  }

  if (!values.slug) {
    errors.slug = "Required";
  }

  return errors;
};

const BoardForm = props => {
  const { handleSubmit, pristine, reset, submitting, invalid } = props;

  return (
    <form onSubmit={handleSubmit} className="uk-form-stacked">
      <fieldset className="uk-fieldset">
        <legend className="uk-legend">Create Board</legend>
        <Field name="name" component={Input} type="text" label="Name" />

        <Field name="slug" component={Input} type="text" label="Slug" />

        <Field name="description" component={Editor} label="Description" />

        <div>
          <button
            className="uk-button uk-button-primary"
            aria-disabled={pristine || submitting}
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Create
          </button>
          <button
            className="uk-button uk-button-danger uk-margin-small-left"
            aria-disabled={pristine || submitting}
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Cancel
          </button>
        </div>
      </fieldset>
    </form>
  );
};

BoardForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
  form: "board",
  validate
})(BoardForm);
