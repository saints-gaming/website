import React from "react";
import PropTypes from "prop-types";

const InputField = ({ className, input, label, meta, placeholder, type }) => {
  return (
    <div className="uk-margin">
      {label &&
        <label className="uk-form-label" htmlFor={input.name}>
          {label}
        </label>}
      <div className="uk-form-controls">
        <input
          className={`uk-input${meta.touched && meta.error
            ? " uk-form-danger"
            : ""}${meta.touched && meta.valid
            ? " uk-form-success"
            : ""} ${className}`}
          {...input}
          type={type}
          placeholder={placeholder}
        />
        {meta.touched &&
          (meta.error || meta.warning) &&
          <p
            className={`uk-text-small uk-margin-remove-top${meta.error
              ? " uk-text-danger"
              : " uk-text-warning"}`}
          >
            {meta.error || meta.warning}
          </p>}
      </div>
    </div>
  );
};

const InputShape = {
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onDragStart: PropTypes.func.isRequired,
  onDrop: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  value: PropTypes.string
};

const MetaShape = {
  active: PropTypes.bool.isRequired,
  asyncValidation: PropTypes.bool,
  autofilled: PropTypes.bool.isRequired,
  dirty: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.string,
  form: PropTypes.string.isRequired,
  initial: PropTypes.string,
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  touched: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
  visited: PropTypes.bool.isRequired,
  warning: PropTypes.string
};
InputField.propTypes = {
  className: PropTypes.string,
  input: PropTypes.shape(InputShape).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape(MetaShape).isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string.isRequired
};

InputField.defaultProps = {
  className: "",
  label: "",
  placeholder: ""
};

export default InputField;
