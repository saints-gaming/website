import React, { Component } from "react";
import PropTypes from "prop-types";
import RichTextEditor from "react-rte";

class EditorField extends Component {
  constructor(props) {
    super(props);
    let initState = RichTextEditor.createEmptyValue();
    if (this.props.input && this.props.input.value) {
      initState = RichTextEditor.createValueFromString(
        this.props.input.value,
        "markdown"
      );
    }

    this.state = {
      value: initState
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    this.setState({ value });
    if (this.props.input && this.props.input.onChange) {
      this.props.input.onChange(value.toString("markdown"));
    }
  }

  render() {
    const { input, label, meta } = this.props;
    return (
      <div className="uk-margin">
        {label &&
          <label className="uk-form-label" htmlFor={input.name}>
            {label}
          </label>}
        <div className="uk-form-controls">
          <RichTextEditor value={this.state.value} onChange={this.onChange} />
          {meta.touched &&
            (meta.error || meta.warning) &&
            <p
              className={`uk-text-small uk-margin-remove-top${meta.error
                ? " uk-text-danger"
                : " uk-text-warning"}`}
            >
              {meta.error || meta.warning}
            </p>}
        </div>
      </div>
    );
  }
}

const InputShape = {
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onDragStart: PropTypes.func.isRequired,
  onDrop: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  value: PropTypes.string
};

const MetaShape = {
  active: PropTypes.bool.isRequired,
  asyncValidation: PropTypes.bool,
  autofilled: PropTypes.bool.isRequired,
  dirty: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.string,
  form: PropTypes.string.isRequired,
  initial: PropTypes.string,
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  touched: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
  visited: PropTypes.bool.isRequired,
  warning: PropTypes.string
};
EditorField.propTypes = {
  className: PropTypes.string,
  input: PropTypes.shape(InputShape).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape(MetaShape).isRequired,
  placeholder: PropTypes.string
};

EditorField.defaultProps = {
  className: "",
  label: "",
  placeholder: ""
};

export default EditorField;
