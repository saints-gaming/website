// @flow
/* eslint react/prop-types: 0 */
/* eslint react/no-unused-prop-types: 0 */
import React from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";
import isEmail from "sane-email-validation";
import Input from "../Fields/Input";

const validate = values => {
  const errors = {};
  if (!values.username) {
    errors.username = "Required";
  }

  if (!values.email) {
    errors.email = "Required";
  } else if (!isEmail(values.email)) {
    errors.email = "Invalid email";
  }

  if (!values.password) {
    errors.password = "Required";
  } else if (values.password.length < 6) {
    errors.password = "The password needs to be 6 letters long";
  }
  return errors;
};

const RegisterForm = props => {
  const { handleSubmit, pristine, reset, submitting, invalid } = props;
  return (
    <form onSubmit={handleSubmit} className="uk-form-stacked">
      <fieldset className="uk-fieldset">
        <legend className="uk-legend">Register</legend>

        <Field name="username" component={Input} type="text" label="Username" />
        <Field name="email" component={Input} type="email" label="Email" />
        <Field
          name="password"
          component={Input}
          type="password"
          label="Password"
        />

        <div>
          <button
            className="uk-button uk-button-primary"
            aria-disabled={pristine || submitting}
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Register
          </button>
          <button
            className="uk-button uk-button-danger uk-margin-small-left"
            aria-disabled={pristine || submitting}
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Cancel
          </button>
        </div>
      </fieldset>
    </form>
  );
};

RegisterForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
  form: "register",
  validate
})(RegisterForm);
