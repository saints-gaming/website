// @flow
/* eslint react/prop-types: 0 */
/* eslint react/no-unused-prop-types: 0 */
import React from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";
import Input from "../Fields/Input";
import Editor from "../Fields/Editor";

const validate = values => {
  const errors = {};
  if (!values.subject) {
    errors.subject = "Required";
  }

  if (!values.content) {
    errors.content = "Required";
  }

  return errors;
};

const BoardForm = props => {
  const { handleSubmit, pristine, reset, submitting, invalid } = props;

  return (
    <form onSubmit={handleSubmit} className="uk-form-stacked">
      <fieldset className="uk-fieldset">
        <legend>Creat thread</legend>
        <Field
          name="subject"
          component={Input}
          label="Subject"
          type="text"
          placeholder="Subject"
        />

        <Field
          name="content"
          component={Editor}
          label="Content"
          placeholder="content"
        />

        <div>
          <button
            className="uk-button uk-button-primary"
            aria-disabled={pristine || submitting}
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Create
          </button>
          <button
            className="uk-button uk-button-danger uk-margin-small-left"
            aria-disabled={pristine || submitting}
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Cancel
          </button>
        </div>
      </fieldset>
    </form>
  );
};

BoardForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
  form: "thread",
  validate
})(BoardForm);
