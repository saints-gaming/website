// @flow
/* eslint react/prop-types: 0 */
/* eslint react/no-unused-prop-types: 0 */
import React from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";
import isEmail from "sane-email-validation";
import Input from "../Fields/Input";

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = "Required";
  } else if (!isEmail(values.email)) {
    errors.email = "Invalid email";
  }

  if (!values.password) {
    errors.password = "Required";
  }
  return errors;
};

const LoginForm = props => {
  const { handleSubmit, pristine, reset, submitting, invalid } = props;

  return (
    <form onSubmit={handleSubmit} className="uk-form-stacked">
      <fieldset className="uk-fieldset">
        <legend className="uk-legend">Login</legend>

        <Field label="Email" name="email" component={Input} type="email" />
        <Field
          label="Password"
          name="password"
          component={Input}
          type="password"
        />

        <div>
          <button
            className="uk-button uk-button-primary"
            aria-disabled={pristine || submitting}
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Login
          </button>
          <button
            className="uk-button uk-button-danger uk-margin-small-left"
            aria-disabled={pristine || submitting}
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Cancel
          </button>
        </div>
      </fieldset>
    </form>
  );
};

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
  form: "login",
  validate
})(LoginForm);
