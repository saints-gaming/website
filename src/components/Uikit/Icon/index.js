import React from "react";
import Isvg from "react-inlinesvg";
import PropTypes from "prop-types";
import icons from "./icons";

const Icon = ({ className, icon }) =>
  <Isvg className={`uk-icon ${className}`} src={icons[icon]}>
    <img className={`uk-icon ${className}`} alt="" src={icons[icon]} />
  </Isvg>;

Icon.propTypes = {
  icon: PropTypes.oneOf(Object.keys(icons)).isRequired,
  className: PropTypes.string
};

Icon.defaultProps = {
  className: ""
};

export default Icon;
