/* global it describe */
import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import PrivateRoute from "./index";

const TestComponent = () => <div>test component</div>;

describe("PrivateRoute", () => {
  it("should render without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <MemoryRouter>
        <PrivateRoute component={TestComponent} />
      </MemoryRouter>,
      div
    );
  });
});
