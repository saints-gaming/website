// @flow

import React from "react";
import { Route, Redirect } from "react-router-dom";
import PropTypes from "prop-types";

const PrivateRoute = ({ component: Component, user, ...rest }) =>
  <Route
    {...rest}
    render={props =>
      user.authenticated
        ? <Component user={user} {...props} />
        : <Redirect to="/login" />}
  />;

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
    .isRequired,
  user: PropTypes.oneOfType([PropTypes.shape(), PropTypes.bool])
};

const defaultProps = {
  user: false
};

PrivateRoute.defaultProps = defaultProps;
export default PrivateRoute;
