// @flow
import React, { Component } from "react";

import { Route, withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import NavigationBar from "../components/Navbar";
import PrivateRoute from "../components/PrivateRoute";
import { actions } from "saints-kernel";

import AccountPartial from "../partials/Account";
import ForumPartial from "../partials/Forum";
import BoardPartial from "../partials/Forum/BoardPartial";
import HomePartial from "../partials/Home";

const menu = [{ label: <span>Forum</span>, link: "/forum", key: "forum" }];
class App extends Component {
  constructor(props) {
    super(props);
    console.log(this);
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {
    this.props.isLoggedIn();
  }

  logout() {
    this.props.doLogout();
  }
  render() {
    return (
      <div className="app">
        <NavigationBar
          menu={menu}
          auth={this.props.user.authenticated}
          user={
            this.props.user.authenticated
              ? this.props.user.details.username
              : ""
          }
          logout={this.logout}
        />
        <PrivateRoute
          user={this.props.user}
          path="/account"
          component={AccountPartial}
        />
        <Route exact path="/forum/:board" component={BoardPartial} />
        <Route exact path="/forum" component={ForumPartial} />
        <Route exact path="/" component={HomePartial} />
      </div>
    );
  }
}

const mapStateToProps = state => ({ user: state.user });
const mapDispatchToProps = dispatch =>
  bindActionCreators(actions.user, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
